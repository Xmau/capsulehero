﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsIndicator : MonoBehaviour
{

    private Text myText;

    // Update is called once per frame
    void Update()
    {
        myText = this.gameObject.GetComponent<Text>();
        myText.text = GameLogic._instance.puntuacion.ToString();

    }
}

