﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsuleBehaviour : MonoBehaviour
{
    private int individualColor;
    private bool status = false;
    
    // Start is called before the first frame update
    void Start()
    {
        individualColor = GameLogic._instance.color;
    }

    // Update is called once per frame
    void Update()
    {
       
       
       
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Ground" && GameLogic._instance.q && individualColor == 0)
        {
            Debug.Log("Amarillo");
            GameLogic._instance.puntuacion += 1;
            status = true;
        }
        else if (other.gameObject.tag == "Ground" && GameLogic._instance.w &&  individualColor == 1)
        {
            Debug.Log("Azul");
            GameLogic._instance.puntuacion += 1;
            status = true;
        }
        else if (other.gameObject.tag == "Ground" && GameLogic._instance.e &&  individualColor == 2)
        {
            Debug.Log("Rojo");
            GameLogic._instance.puntuacion += 1;
            status = true;
        }
        else if (other.gameObject.tag == "Ground" && GameLogic._instance.r &&  individualColor == 3)
        {
            Debug.Log("Verde");            
            GameLogic._instance.puntuacion += 1;
            status = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Destroy(gameObject);
        if (status == false)
        {
            GameLogic._instance.puntuacion -= 1;
            status = false;
        }
    }
}
