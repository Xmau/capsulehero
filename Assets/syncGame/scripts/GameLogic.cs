﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Random = System.Random;

public class GameLogic : MonoBehaviour
{
    
    
    public static GameLogic _instance;

    public GameObject yellowCapsule;
    public GameObject blue;
    public GameObject redCapsule;
    public GameObject greenCapsule;
    private float actualTime;
    Random rnd;
    private int randomSpawner;
    public bool q = false;
    public bool w = false;
    public bool e = false;
    public bool r = false;
    
    public int color;

    public int puntuacion = 0;
    

    public void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        } else
        {
            Destroy(this);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        actualTime = Time.time;
        rnd = new Random();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            q = true;
        }
        else
        {
            q = false;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            w = true;
        }
        else
        {
            w = false;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            e = true;
        }
        else
        {
            e = false;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            r = true;
        }
        else
        {
            r = false;
        }
        
        if (Time.time >= actualTime + 0.8f)
        {
            randomSpawner = rnd.Next(0, 4);
            switch (randomSpawner)
            {
                case 0:
                    Instantiate(yellowCapsule);
                    color = 0;
                    break;
                case 1:
                    Instantiate(blue);
                    color = 1;
                    break;
                case 2:
                    Instantiate(redCapsule);
                    color = 2;
                    break;
                default:
                    Instantiate(greenCapsule);
                    color = 3;
                    break;
            }
            actualTime = Time.time;
        }
    }
}
